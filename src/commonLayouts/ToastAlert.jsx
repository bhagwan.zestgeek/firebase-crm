import React from 'react';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ToastAlert = (props) => {
    function NotifyToast() {
        toast.error("Password validation failed!");
    }
    return (
        <>
            <div className="App">
                <div className="btn-group">
                    <NotifyToast/>
                </div>
            </div>
            <ToastContainer />
        </>
    );
}

export default ToastAlert;