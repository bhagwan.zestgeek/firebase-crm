import React from 'react';
import { Pie, Doughnut } from 'react-chartjs-2';

import Chart from 'chart.js/auto';

const PieChart = () => {

    const state = {
        labels: ['America', 'Asia', 'Europe', 'Africa'],
        datasets: [
            {
                label: 'Current Visits',
                backgroundColor: [
                    '#4caf50',
                    '#2196f3',
                    '#ffeb3b',
                    '#f44336'
                ],
                hoverBackgroundColor: [
                    '#501800',
                    '#4B5000',
                    '#175000',
                    '#003350'
                ],
                data: [27.7, 34.7, 9.2, 28.4],
                radius: '75%',
            }
        ],
    }
    return (
        <>
            <div>
                <Pie
                    data={state}
                    options={{
                        title: {
                            display: false,
                            text: 'Current Visits',
                            fontSize: 20
                        },
                        legend: {
                            display: false,
                            position: 'right'
                        },
                        responsive: true,
                        maintainAspectRatio: true,
                    }}
                    
                />
                
            </div>
        </>
    );
}

export default PieChart;