import React from 'react';
import Container from '@mui/material/Container';
import { Box } from '@mui/system';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/material/Grid';
import FacebookOutlinedIcon from '@mui/icons-material/FacebookOutlined';
import GoogleIcon from '@mui/icons-material/Google';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import TwitterIcon from '@mui/icons-material/Twitter';
import { Typography } from '@mui/material';


const useStyles = makeStyles({
    buttonGroup: {
        float: 'right'
    },
    graphContainer: {
        borderRadius: '10px',
        padding: '20px',
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
    },
    graphButtonColor: {
        color: '#000000',
    },
})


const TrafficBySite = () => {

    const classes = useStyles();

    return (
        <>
            <Container className={classes.graphContainer} >
                {/* Box for Component Title */}
                <Box >
                    <Typography variant="h5" component="h5">
                        Traffic by Site
                    </Typography>
                </Box>
                {/* Box for Component Timeline */}
                <Box sx={{ width: '100%' }}>
                    <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                        <Grid item xs={6}>
                            <Box className={classes.graphContainer}>
                                <FacebookOutlinedIcon/>
                                Facebook
                            </Box>
                        </Grid>
                        <Grid item xs={6}>
                            <Box className={classes.graphContainer}>
                                <GoogleIcon/>
                                Google
                            </Box>
                        </Grid>
                        <Grid item xs={6}>
                            <Box className={classes.graphContainer}>
                                <LinkedInIcon/>
                                Linkedin
                            </Box>
                        </Grid>
                        <Grid item xs={6}>
                            <Box className={classes.graphContainer}>
                                <TwitterIcon />
                                Twitter
                            </Box>
                        </Grid>
                    </Grid>
                </Box>

            </Container>


        </>
    );

}

export default TrafficBySite;