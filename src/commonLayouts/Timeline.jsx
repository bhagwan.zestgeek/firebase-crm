
import * as React from 'react';
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineOppositeContent from '@mui/lab/TimelineOppositeContent';
import TimelineDot from '@mui/lab/TimelineDot';
import FastfoodIcon from '@mui/icons-material/Fastfood';
import LaptopMacIcon from '@mui/icons-material/LaptopMac';
import HotelIcon from '@mui/icons-material/Hotel';
import RepeatIcon from '@mui/icons-material/Repeat';
import Typography from '@mui/material/Typography';

export default function CustomizedTimeline() {

	const data = [
		{ 'id': 1, title: 'Zestgeek', 'date': '17 July 2016' },
		{ 'id': 2, title: 'ZestGeek', 'date': '23 August 2021' },
		{ 'id': 3, title: 'Zestgeek', 'date': '05 September 2016' },
		{ 'id': 4, title: 'Zestgeek', 'date': '18 March 2018' },
		{ 'id': 5, title: 'Zestgeek', 'date': '31 January 2019' },
	]


	return (
		<Timeline position="right">
			{
				data.map((item) => (
					<>
						<TimelineItem sx={{ marginRight: '200px' }}>
							<TimelineSeparator>
								<TimelineConnector />
								<TimelineDot sx={{ color: 'red' }} />
								<TimelineConnector />
							</TimelineSeparator>
							<TimelineContent sx={{ py: '12px', px: 2 }}>
								<Typography sx={{ fontSize:'12px' }}>
								{ item.title }
								</Typography>
								<Typography sx={{ fontSize: '12px', width: '150px' }}>{ item.date }</Typography>
							</TimelineContent>
						</TimelineItem>
					</>
				))
			}
		</Timeline>
	);
}
