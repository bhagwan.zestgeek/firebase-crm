import React from 'react';

import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';



const Copyright = (props) => {
    
	return (
		<Typography variant="body2" color="text.secondary" align="center" {...props}>
			{'Copyright © '}
			<div>
				<a color="inherit" href="https://Zestgeek.com/">
					ZestGeek Solutions
				</a>{' '}
			</div>	
			{new Date().getFullYear()}
			{'.'}
		</Typography>
	);
}


export default Copyright;