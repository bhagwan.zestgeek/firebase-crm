import React from 'react';
import Box from '@mui/material/Box';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
// import AdbIcon from '@mui/icons-material/Adb';
const Item = styled(Paper)(({ theme, background }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height:'238px' ,
    borderRadius:'10px',
    background: background || 'rgb(200, 250, 205)',
    minWidth:'235px',
    position:'relative',
    display: 'flex',
    justifyContent:'center',
  }));
const useStyles=makeStyles({
  main:{
    width:'80%',
    display: 'flex',
    justifyContent:'center',
    direction: 'Column',
  },
  icon:{
    position: 'absolute',
    top:'15px',
    right:'18px',
  },
  circle:{
    position: 'absolute',
    width: '60px',
    height: '60px',
    top:'50px',
     border: '2px solid linear-gradient(135deg,rgba(0, 123, 85, 0) 0%, rgba(0, 123, 85, 0.24) 100%)',
    borderRadius:'50%',
    color:'rgb(0, 123, 85)',
    backgroundImage: 'linear-gradient(135deg,rgba(0, 123, 85, 0) 0%, rgba(0, 123, 85, 0.24) 100%)',
  },
  heading:{
    position: 'absolute',
    top:'100px',
  }
})
const  Card= ({
  Icon,
  number,
  heading,
  background,
  border,
  borderImage,
  color,
  inProps,
  props,
  icon,
})=>{
    const classes=useStyles({color});
    return (
       <>
          <Box container spacing={2}  md={2} xs={2}  >
          <Box item xs={2} sm={2}  >  
            <Item background={background}>
              <Box className={classes.main} >
              <Box  className={classes.circle}>
              <div className={classes.icon}>
              {icon}
                </div>
            </Box>
            <Box className={classes.heading}>
            <h2 sx={{margin:'0px'}}>{number}</h2>
              <p sx={{margin:'0px', color:'red'}}>{heading}</p>
            </Box>
              </Box>  
            </Item>
          </Box>
        </Box>
 </>
    )
}

export default Card
