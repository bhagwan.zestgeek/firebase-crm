import React from 'react';
import { Line } from 'react-chartjs-2';

import Chart from 'chart.js/auto';




const MultiTypeChart = () => {

  const labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul']
  const data = {
    labels: labels,
    datasets: [{
      label: 'Team A',
      data: [23,43, 38,52, 44, 96, 62, 32],
      fill: false,
      borderColor: 'rgb(75, 192, 192)',
      tension: 0.3,

      fill: false,          // Don't fill area under the line
          borderColor: 'green'  // Line color
    }],
  };

  return (
    <>
      <Line
        data={data}
        options={{
          title: {
              display: false,
              text: 'Current Visits',
              fontSize: 20
          },
          legend: {
              display: false,
              position: 'right'
          },
          responsive: true,
          maintainAspectRatio: true,
      }}
      />
    </>
  );
}

export default MultiTypeChart;