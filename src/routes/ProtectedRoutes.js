import React from "react";
import { Route, Link } from "react-router";
import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import { getAuth } from 'firebase/auth';


const ProtectedRoute = ({ children }) => {
	
	const user = useSelector((state) => state.auth.value);
	// // const user = false;
	// console.log("Logged In : ", user);

	// const isAuth = localStorage.getItem('isAuth')
	// const user = getAuth();
	console.log(user, "user");

	
	return user ? children : <Navigate to='/' />

	
}
export default ProtectedRoute;
