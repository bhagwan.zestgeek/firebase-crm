import React from 'react';
import '../App.css'
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import ListItemButton from '@mui/material/ListItemButton';
import SendIcon from '@mui/icons-material/Send';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { useNavigate } from 'react-router-dom';
import Card from '../commonLayouts/Card';
import { AppleIcon, WindowIcon, Error, Android } from '../assets/svg'
import { getAuth, signOut } from 'firebase/auth';
import useMediaQuery from '@mui/material/useMediaQuery';
import PeopleOutlineOutlinedIcon from '@mui/icons-material/PeopleOutlineOutlined';
import InsertChartOutlinedRoundedIcon from '@mui/icons-material/InsertChartOutlinedRounded';
import Container from '@mui/material/Container';
import WebsiteVisit from '../components/Dashboard/WebsiteVisit';
import CurrentVisits from '../components/Dashboard/CurrentVisits';
import OrderTimeline from '../components/Dashboard/OrderTimeline';
import NewsUpdate from '../commonLayouts/NewsUpdate'
import TrafficBySite from '../commonLayouts/TraficBySIte';


const drawerWidth = 240;

const CardDetails = [
	{
		icon: <Android />,
		heading: 'Weekly Sales',
		number: '714K',
		background: 'rgb(200, 250, 205)',

	},
	{
		icon: <AppleIcon />,
		heading: 'New User',
		number: '1.35M',
		background: 'rgb(208, 242, 255)'
	},
	{
		icon: <WindowIcon />,
		heading: 'Item orders',
		number: '1.76M',
		background: 'rgb(255, 247, 205)'
	},
	{
		icon: <Error />,
		heading: 'Bug Reports',
		number: '1.98M',
		background: 'rgb(255, 231, 217)'
	}
]
const openedMixin = (theme) => ({
	width: drawerWidth,
	transition: theme.transitions.create('width', {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.enteringScreen,
	}),
	overflowX: 'hidden',
});

const closedMixin = (theme) => ({
	transition: theme.transitions.create('width', {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	overflowX: 'hidden',
	width: `calc(${theme.spacing(7)} + 1px)`,
	[theme.breakpoints.up('sm')]: {
		width: `calc(${theme.spacing(9)} + 1px)`,
	},
});

const DrawerHeader = styled('div')(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'flex-end',
	padding: theme.spacing(0, 1),
	// necessary for content to be below app bar
	...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
	shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
	zIndex: theme.zIndex.drawer + 1,
	transition: theme.transitions.create(['width', 'margin'], {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	...(open && {
		marginLeft: drawerWidth,
		width: `calc(100% - ${drawerWidth}px)`,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	}),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
	({ theme, open }) => ({
		width: drawerWidth,
		flexShrink: 0,
		whiteSpace: 'nowrap',
		boxSizing: 'border-box',
		...(open && {
			...openedMixin(theme),
			'& .MuiDrawer-paper': openedMixin(theme),
		}),
		...(!open && {
			...closedMixin(theme),
			'& .MuiDrawer-paper': closedMixin(theme),
		}),
	}),
);

const Dashboard = () => {
	const navigate = useNavigate();

	const [auth, setAuth] = React.useState(true);
	const [anchorEl, setAnchorEl] = React.useState(null);

	const theme = useTheme();
	const [open, setOpen] = React.useState(false);

	const handleDrawerOpen = () => {
		setOpen(true);
	};

	const handleDrawerClose = () => {
		setOpen(false);
	};

	const handleChange = (event) => {
		setAuth(event.target.checked);
	};

	const handleMenu = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		navigate('/')
		setAnchorEl(null);
	};
	const handleSignOut = (e) => {
		e.preventDefault()
		signOut(auth).then(() => {
			console.log("Sign-out successful.")
			// Sign-out successful.
			navigate('/')
		}).catch((error) => {
			// An error happened.
			console.log("Something went wrong on login out user.");
			navigate('/dashboard');
		});
	}

	const change = () => {
		navigate('/user')
	}

	return (
		<Box className="Dashboard_container" maxWidth="100%" >
			<CssBaseline />
			<AppBar position="fixed" open={open}>
				<Toolbar>
					<IconButton
						color="inherit"
						aria-label="open drawer"
						onClick={handleDrawerOpen}
						edge="start"
						sx={{
							marginRight: '36px',
							...(open && { display: 'none' }),
						}}
					>
						<MenuIcon />
					</IconButton>
					<Typography variant="h6" noWrap component="div">
						CRM
					</Typography>
					{auth && (
						<div>
							<IconButton
								size="large"
								aria-label="account of current user"
								aria-controls="menu-appbar"
								aria-haspopup="true"
								onClick={handleMenu}
								color="inherit"
							>
								<AccountCircle />
							</IconButton>
							<Menu
								id="menu-appbar"
								anchorEl={anchorEl}
								anchorOrigin={{
									vertical: 'top',
									horizontal: 'right',
								}}
								keepMounted
								transformOrigin={{
									vertical: 'top',
									horizontal: 'right',
								}}
								open={Boolean(anchorEl)}
							>
								<MenuItem onClick={handleClose}>Logout</MenuItem>
							</Menu>
						</div>
					)}
				</Toolbar>
			</AppBar>
			<Drawer variant="permanent" open={open}>
				<DrawerHeader>
					<IconButton onClick={handleDrawerClose}>
						{theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
					</IconButton>
				</DrawerHeader>
				<Divider />
				<List>
					<ListItemButton >
						<ListItemIcon>
							<SendIcon />
						</ListItemIcon>
						<ListItemText primary="Dashboard" />
					</ListItemButton>
					<ListItemButton onClick={change}>
						<ListItemIcon>
							<SendIcon />
						</ListItemIcon>
						<ListItemText primary="Sent mail" />
					</ListItemButton>
				</List>
				<Divider />
				<List>
					{['All mail', 'Trash', 'Spam'].map((text, index) => (
						<ListItem button key={text}>
							<ListItemIcon>
								{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
							</ListItemIcon>
							<ListItemText primary={text} />
						</ListItem>
					))}
				</List>
			</Drawer>
			<Box className="drawer" component="main" sx={{ flexGrow: 1, p: 3 }}>
				<DrawerHeader />
			</Box>

			<Box w='full' sx={{ marginLeft: '80px', padding: '10px' }}>
				<Typography variant="h4" component="h4">
					<strong>Hi, Welcome back!</strong>
				</Typography>
			</Box>

			<Container sx={{ padding: '10px' }}>
				<div className=""></div>
			</Container>

			<Box w="full" sx={{ display: 'flex', marginLeft: '80px', flexDirection: 'row', gap: '2px' }}>
				{/* same as below map */}
			</Box>
    return (
        <Box className="Dashboard_container"  >
            <CssBaseline />
            <AppBar position="fixed" open={open}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{
                            marginRight: '36px',
                            ...(open && { display: 'none' }),
                        }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap component="div">
                        CRM
                    </Typography>
                    {auth && (
                        <div>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleMenu}
                                color="inherit"
                            >
                                <AccountCircle />
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorEl)}
                            // onClose={handleClose}
                            >
                                {/* <MenuItem onClick={handleClose}>Profile</MenuItem> */}
                                <MenuItem onClick={handleSignOut}>Logout</MenuItem>
                            </Menu>
                        </div>
                    )}
                </Toolbar>
            </AppBar>
            <Drawer variant="permanent" open={open}>
                <DrawerHeader>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                </DrawerHeader>
                <Divider />
                <List>
                    <ListItemButton >
                        <ListItemIcon>
                            <InsertChartOutlinedRoundedIcon />
                        </ListItemIcon>
                        <ListItemText primary="Dashboard" />
                    </ListItemButton>
                    <ListItemButton onClick={change}>
                        <ListItemIcon>
                            <PeopleOutlineOutlinedIcon />
                        </ListItemIcon>
                        <ListItemText primary="Users" />
                    </ListItemButton>
                </List>
                <Divider />
                <List>
                    {['All mail', 'Trash', 'Spam'].map((text, index) => (
                        <ListItem button key={text}>
                            <ListItemIcon>
                                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                            </ListItemIcon>
                            <ListItemText primary={text} />
                        </ListItem>
                    ))}
                </List>
            </Drawer>
            <Box className="drawer" component="main" sx={{ flexGrow: 1, p: 3 }}>
                <DrawerHeader />
            </Box>
            <Box  sx={{ display: 'flex',rowGap:'20px', justifyContent:''  }}  
             marginLeft={{sm:'', md:'', lg:'300px'}} 
            justifyContent={{xs:'center', sm:'center', md:'center'}}
            columnGap={{xs:'20px', sm:'20px', md:'20px'}}
            flexDirection={{xs:'column',sm:'column',md:'row', lg:'row'}} 
            >
                {
                    CardDetails.map((item) => (
                        <Card heading={item.heading} number={item.number} icon={item.icon} background={item.background} />
                    ))
                }
            </Box>
        </Box>
    );
		

			<Box w='full' sx={{ padding: '20px', height: '500px' }}>
				<Container style={{ display: "grid", gridTemplateColumns: "60% 40%", gridGap: '50px', alignItems: "center", justifyContent: "center", height: '250px' }}>
					<WebsiteVisit />
					<CurrentVisits />
				</Container>
			</Box>

			<Box w='full' sx={{ padding: '20px' }}>
				<Container style={{ display: "grid", gridTemplateColumns: "60% 40%", gridGap: '50px', alignItems: "center", justifyContent: "center" }}>
					<WebsiteVisit />
					<OrderTimeline />
				</Container>
			</Box>

            <Box w='full' sx={{ padding: '20px', height: '500px' }}>
            <Container style={{ display: "grid", gridTemplateColumns: "60% 40%", gridGap: '50px', alignItems: "center", justifyContent: "center", height: '250px' }}>
                    <NewsUpdate />
				</Container>
            </Box>
			<Box w='full' sx={{ padding: '20px' }}>
				<Container style={{ display: "grid", gridTemplateColumns: "40% 60%", gridGap: '50px', alignItems: "center", justifyContent: "center" }}>
					<TrafficBySite />
				</Container>
			</Box>

		</Box>
	);
}
export default Dashboard;