import React, { useState } from 'react';

import { useNavigate } from 'react-router-dom';

import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth';

// import ToastAlert from '../commonLayouts/ToastAlert';

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const useAuthLogin = () => {
    const auth = getAuth();
    const navigate = useNavigate();

    const [user, setUser] = useState({
        fname : '',
        lname : '',
        email : '',
        password : '',
    });
    const handleSubmit = (event) => {
        event.preventDefault();
        
        const data = new FormData(event.currentTarget);
       
		let form_data = {
			email : data.get('email'),
			password : data.get('password'),
			passwordConfirm : data.get('passwordConfirm'),
			fname : data.get('fname'),
			lname : data.get('lname')
		}

		if (form_data.fname === null  && form_data.lname === null && form_data.email !== null && form_data.password !== null){
			console.log("login event....");
            // Signing user on firebase 
            signInWithEmailAndPassword(auth, form_data.email, form_data.password)
                .then((userCredential) => {
                    const user = userCredential.user;
                    console.log("Singed in Successfully: ", user);
                    // localStorage.setItem('isAuth', true)
                    navigate('/dashboard');
                })
                .catch((error) => {
                    const errorCode = error.code;
                    const errorMessage = error.message;
                    console.log("An error occured: ", errorCode, errorMessage);
                    // navigate('/')
                    toast.error(errorMessage);

                });
		}

		if (form_data.fname !== null  && form_data.lname !== null){
			console.log("register event....");
            if (form_data.password === form_data.passwordConfirm){
                console.log("Signing to firebase...");
                // Registering user on firebase
                createUserWithEmailAndPassword(auth, data.get('email'), data.get('password'))
                    .then((userCredential) => {
                        const user = userCredential.user;
                        console.log("Registered Successfully! ", user);
                    })
                    .catch((error) => {
                        const errorCode = error.code;
                        const errorMessage = error.message;
                        console.log("Error ocured: ", errorCode, errorMessage);                      
                    });
            }else{
                console.log("password not match!!! please use same password...");
                toast.error("Password Validation failed!");
                console.log("toast done");
            }   
		}

    };

    const handleChange = (e) => {
        e.preventDefault();
        const {target} = e;
        setUser({
            ...user,
            [target.name] : target.value
        })
    }
    return {
        handleSubmit,
        user,
        setUser,
        handleChange
    };
}


export default useAuthLogin;



