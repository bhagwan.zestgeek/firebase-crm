import React from 'react';

import Container from '@mui/material/Container';
import { Box } from '@mui/system';
import { Typography } from '@mui/material';

import { makeStyles } from '@mui/styles';


import PieChart from '../../commonLayouts/PieChart';



const useStyles = makeStyles({
    buttonGroup:{
        float: 'right'
    },
    graphContainer: {
        // border: '0px solid #9e9e9e',
        borderRadius: '10px',
        padding: '20px',
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
        height: '467px',
    },
    graphButtonColor: {
        color: '#000000',
    },
})


const WebsiteVisit = () => {

    const classes = useStyles();

    return (
        <>
            <Container className={classes.graphContainer} >
                {/* Box for Component Title */}
                <Box >
                    <Typography variant="h5" component="h5">
                        Current Visits
                    </Typography>
                </Box>
                {/* Box for Component Graph */}
                <Box >
                    <PieChart/>
                </Box>
            </Container>


        </>
    );

}

export default WebsiteVisit;