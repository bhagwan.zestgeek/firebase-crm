import React from 'react';
import Container from '@mui/material/Container';
import { Box } from '@mui/system';
import { Typography } from '@mui/material';
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import Brightness1Icon from '@mui/icons-material/Brightness1';
import { makeStyles } from '@mui/styles';
import MultiTypeChart from '../../commonLayouts/MultiTypeChart';

const useStyles = makeStyles({
    buttonGroup:{
        float: 'right'
    },
    graphContainer: {
        // border: '0px solid #9e9e9e',
        borderRadius: '10px',
        padding: '30px',
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
        maxHeight: '500px',
    },
    graphButtonColor: {
        color: '#000000',
    },
})

const WebsiteVisit = () => {
    const classes = useStyles();
    const button = [
        {id: '1', name:'Team A', iconColor: 'success'},
        {id: '1', name:'Team B', iconColor: 'error'},
        {id: '1', name:'Team C', iconColor: 'info'},
    ]
    return (
        <>
            <Container className={classes.graphContainer} >
                {/* Box for Component Title */}
                <Box >
                    <Typography variant="h5" component="h5">
                        Website Visits
                    </Typography>
                    <Typography varient='h6' component='h6'>
                    (+43%) than last year
                    </Typography>
                </Box>

                {/* Box for Component Action Button */}
                <Box className={classes.buttonGroup}>
                    <ButtonGroup >
                        {
                            button.map((item) => {
                                return <Button variant="text" size="medium" color={item.iconColor} startIcon={<Brightness1Icon />}> <span className={classes.graphButtonColor}>{ item.name }</span> </Button>;
                            })
                        }
                    </ButtonGroup>
                </Box>

                {/* Box for Component Graph */}
                <Box>
                    <MultiTypeChart options={{ maintainAspectRatio: false }}/> 
                </Box>
            </Container>
        </>
    );
}

export default WebsiteVisit;