import React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';

const Home = () => {
    const navigate = useNavigate();
    return (
        <>
            <Container varient='primary' >
                <Typography variant="h5" component="div" gutterBottom>
                    React - Firebase CRM -by Zestgeek Solutions
                </Typography>
                <hr/>
                <hr/>
                <hr/>
                <hr/>
                <hr/>
                <div align="center" >
                        <Button onClick={() => navigate('/login')}>Login</Button>
                </div>
            </Container>
        </>
    );
}
export default Home;