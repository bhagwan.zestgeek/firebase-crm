import  React,{useEffect} from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import { useNavigate } from 'react-router-dom';
import TablePagination from '@mui/material/TablePagination';
import axios from 'axios'
function createData(name, email, phone, address, id){
	return { name, email, phone, address, id };
}

export default function AcccessibleTable() {
	const navigate = useNavigate()
	const [user,setUser]=React.useState([])

  useEffect(()=>{
	axios.get('https://crmz-e12b4-default-rtdb.asia-southeast1.firebasedatabase.app/userDatabase.json')
	.then(res=>{
		console.log(res,"sdsadsds=>>>")
		 const responseData=res.data;
		 const Adduser=[]
		for(var key in responseData){
			Adduser.push(responseData[key])
		}
		setUser(Adduser)
		console.log(user,"asdsdsdasdsdasds")
		console.log(user,"modifiedUsermodifiedUser")
		console.log("useruseruser", user)	
	})
  },[])

	const handleEditUser = (e, id) => {
		e.preventDefault();
		console.log("handling edit User");
		navigate(`/user/edit/${id}`);
	}
	const handleDeleteUser = (e, id) => {
		e.preventDefault();
		console.log("handling delete User");
		navigate(`/user/delete/${id}`);
	}
	const handleUserClick = (e, id) => {
		e.preventDefault();
		console.log("handling User view");
		navigate(`/user/${id}`);
	}

	const AddUser=()=>{
		navigate('/AddUser')
	}
	return (
		<Paper sx={{ width: '100%', overflow: 'hidden' }}>
		<Button onClick={AddUser} > Add user</Button> 
		<TableContainer component={Paper}>
			<Table sx={{ minWidth: 650 }} aria-label="caption table">
				<TableHead>
					<TableRow>
						<TableCell align="left">Name</TableCell>
						<TableCell align="center">UserId</TableCell>
						<TableCell align="center">Status</TableCell>
						<TableCell align="center">Actions</TableCell>
					</TableRow>
				</TableHead>			
				<TableBody>
					{user.map((row,index) => (
						<TableRow key={index}>
							<TableCell component="th" scope="row">
								<Button onClick={(e) => handleUserClick(e, row.id)}> {row.name} </Button>
							</TableCell>
							<TableCell align="center">{row.id}</TableCell>
							<TableCell align="center">{row.status} </TableCell>
							<TableCell align="center">{row.role}</TableCell>

							<TableCell align="center">
								<ButtonGroup variant="text" aria-label="text button group">
									<Button onClick={(e) => handleEditUser(e, row.id )} color="info"><EditIcon/></Button>
									<Button onClick={(e) => handleEditUser(e, row.id )}  color="error"><DeleteIcon/></Button>
								</ButtonGroup>
							</TableCell>
						</TableRow>
					))}
				</TableBody>
			</Table>
		</TableContainer>
		{/* <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      /> */}
		</Paper>
		
	)
}
