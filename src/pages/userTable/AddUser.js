
import React, { useState } from "react";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import useAuthLogin from '../../hooks/useAuthHook';
import firebase from '../../app/firebaseConfig'
export default function AddUser() {

    const userData=firebase.database().ref("userDatabase");

    const[user,setUser]=useState({
        id:'',
        name:'',
        role:'',
        status:'',
    }) 
    const {id,name,role,status}=user
    const handleSubmit=(e)=>{
        setUser({...user,[e.target.name]:e.target.value});
        console.log(user,"useruser")    
    }
     const onSubmite=()=>{
        firebaseDatabase(id,name,role,status)
        console.log(id,"firebaseDatabasefirebaseDatabase")
     }
    const firebaseDatabase=(id,name,role,status)=>{
            var newUserRef=userData.push();
        newUserRef.set({
            id:id,
            name:name,
            role:role,
            status:status
        })
    }
	return (
			<Grid container component="main" sx={{ height: '100vh' }}>
				<Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
					<Box
						sx={{
							my: 8,
							mx: 4,
							display: 'flex',
							flexDirection: 'column',
							alignItems: 'center',
						}}
					>
						<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
							<LockOutlinedIcon />
						</Avatar>
						<Typography component="h1" variant="h5">
                            Add users
						</Typography>
						<Box component="form" noValidate onSubmit={e=>e.preventDefault()} sx={{ mt: 1 }}>
                        <TextField
								margin="normal"
								fullWidth
								id="name"
								label=" Name"
								type="text"
								name="name"
								autoFocus
								required
                                onChangeCapture={handleSubmit}
							/>
                            <TextField
								margin="normal"
								fullWidth
								id="role"
								label="Role"
								type="text"
								name="role"
								autoFocus
								required
								onChange = { handleSubmit }
							/>

							<TextField
								margin="normal"
								fullWidth
								id="id"
								label="UserID"
								type="number"
								name="id"
								autoComplete="userid"
								autoFocus
								required
								onChange = { handleSubmit }
							/>
							<TextField
								margin="normal"
								fullWidth
								name="status"
								label="Status"
								type="string"
								id="status"
								onChange={handleSubmit}
								required
							/>	
							<Button
								type="submit"
								fullWidth
								variant="contained"
								sx={{ mt: 3, mb: 2 }}
								 onClick={onSubmite}
							>
								Sign In
							</Button>
                        	
						</Box>
					</Box>
				</Grid>
			</Grid>
	);
}