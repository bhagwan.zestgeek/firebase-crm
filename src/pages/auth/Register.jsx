
import React, { useState } from "react";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';


import useAuthLogin from '../../hooks/useAuthHook';
import { getAuth } from "firebase/auth";

import Copyright from '../../commonLayouts/Copyright';
import { useNavigate } from "react-router-dom";

import theme from '../../app/muiTheme';

import { ToastContainer, toast } from "react-toastify";




export default function Register() {

	const { handleSubmit, handleChange } = useAuthLogin();

    const notify = () => {
		toast("Default Notification !");
  
		toast.success("Success Notification !", {
		  position: toast.POSITION.TOP_CENTER
		});
  
		toast.error("Error Notification !", {
		  position: toast.POSITION.TOP_LEFT
		});
  
		toast.warn("Warning Notification !", {
		  position: toast.POSITION.BOTTOM_LEFT
		});
  
		toast.info("Info Notification !", {
		  position: toast.POSITION.BOTTOM_CENTER
		});
  
		toast("Custom Style Notification with css class!", {
		  position: toast.POSITION.BOTTOM_RIGHT,
		  className: 'foo-bar'
		});
	  };
	
	return (
		// <ThemeProvider theme={theme}>
			<Grid container component="main" sx={{ height: '100vh' }}>
				{/* <CssBaseline /> */}
				<Grid
					item
					xs={false}
					sm={4}
					md={7}
					sx={{
						backgroundImage: 'url(https://source.unsplash.com/random)',
						backgroundRepeat: 'no-repeat',
						backgroundColor: (t) =>
							t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
						backgroundSize: 'cover',
						backgroundPosition: 'center',
					}}
				/>
				<Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
					<Box
						sx={{
							my: 8,
							mx: 4,
							display: 'flex',
							flexDirection: 'column',
							alignItems: 'center',
						}}
					>
						<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
							<LockOutlinedIcon />
						</Avatar>
						<Typography component="h1" variant="h5">
                            Register 
						</Typography>
						<Box component="form" noValidate onSubmit={(e) =>handleSubmit(e)} sx={{ mt: 1 }}>
                        <TextField
								margin="normal"
								fullWidth
								id="fname"
								label="First Name"
								type="text"
								name="fname"
								autoFocus
								required
								// onChange = { (e) => setUser({...user, fname: e.target.value}) }
                                onChangeCapture={ (e) => handleChange(e) }
							/>
                            <TextField
								margin="normal"
								fullWidth
								id="lname"
								label="Last Name"
								type="text"
								name="lname"
								autoFocus
								required
								onChange = { handleChange }
							/>

							<TextField
								margin="normal"
								fullWidth
								id="email"
								label="Email Address"
								type="email"
								name="email"
								autoComplete="email"
								autoFocus
								required
								onChange = { handleChange }
							/>
							<TextField
								margin="normal"
								fullWidth
								name="password"
								label="Password"
								type="password"
								id="password"
								autoComplete="current-password"
								onChange={handleChange}
								required
							/>
                            <TextField
								margin="normal"
								fullWidth
								name="passwordConfirm"
								label="Confirm Password"
								type="password"
								id="passwordConfirm"
								autoComplete="current-password"
								onChange={handleChange}
								required
							/>
							
							<Button
								type="submit"
								fullWidth
								variant="contained"
								sx={{ mt: 3, mb: 2 }}
								// onClick={(e) =>handleSubmit(e)}
							>
								Sign In
							</Button>
                            <Grid container>
								<Grid item>
									<Link href="/" variant="body2">
										{"have an account? Sign In"}
									</Link>
								</Grid>

								{/* <button onClick={notify}>Notify</button> */}
								<ToastContainer/>
								
							</Grid>
							
						</Box>
					</Box>
				</Grid>
			</Grid>
	);
}