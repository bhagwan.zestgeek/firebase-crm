import React, { Suspense, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { saveUser } from "./redux/slices/authSlice";
import { Routes, Route, useNavigate } from "react-router-dom";
import { getAuth, onAuthStateChanged } from "firebase/auth";
// import { initializeApp } from "firebase/app";
// import { firebaseConfig } from "./app/firebaseConfig";
import firebaseStore from './app/firebaseConfig'
import Loader from './commonLayouts/Loader';

const Home = React.lazy(() => import('./pages/Home'));
const Login = React.lazy(() => import('./pages/auth/Login'));
const Register = React.lazy(() => import('./pages/auth/Register'));
const Reset = React.lazy(() => import('./pages/auth/Reset'));
const Dashboard = React.lazy(() => import('./routes/Dashboard'));
const ProtectedRoute = React.lazy(() => import('./routes/ProtectedRoutes'));
const UserTable=React.lazy(()=>import('./pages/userTable/UserTable'))
const AddUser=React.lazy(()=>import('./pages/userTable/AddUser'))
function App() {
	// Initializing Firebase app with our crm app configuratio
	// initializeApp(firebaseConfig);

	// const user = useSelector((state) => state.auth.value);
	// console.log("user from state", user);

	const auth = getAuth();
	const navigate = useNavigate();
	const dispatch = useDispatch();

	useEffect(() => {
		onAuthStateChanged(auth, (user) => {
			if (user) {
				console.log("Logged In User: ", user);
				dispatch(saveUser(user));
				navigate("/dashboard");

			} else {
				console.log("No user found or logged out.");
				dispatch(saveUser(undefined));
				navigate("/");
			}
		});
	}, [auth, dispatch]);
	return (
			<Suspense fallback={ <Loader/> }>
				<Routes>
					{/* <Route exact path="/" element={<Home />} /> */}
					<Route exact path="/" element={<Login />} />
					<Route exact path="/register" element={<Register />} />
					<Route exact path="/reset" element={<Reset />} />
					<Route exact path="/user" element={<UserTable />} />
					<Route exact path="/adduser" element={<AddUser />} />
					<Route exact path='/dashboard' element={<Dashboard/>}/>
					{/* <Route exact path='/dashboard' element={ <ProtectedRoute> <Dashboard/> </ProtectedRoute> }/> */}
				</Routes>
			</Suspense>
	);
}

export default App;
